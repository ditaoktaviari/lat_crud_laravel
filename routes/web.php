<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [App\Http\Controllers\MahasiswaController::class,'tampilMahasiswa'])->name('tampil_mhs');

Route::get('/tambah_mhs', [App\Http\Controllers\MahasiswaController::class,'tambahMahasiswa'])->name('tambah_mhs');

Route::post('/prosesTambah', [App\Http\Controllers\MahasiswaController::class,'prosesTambah'])->name('tambah');

Route::get('/getId{id}', [App\Http\Controllers\MahasiswaController::class,'getId'])->name('edit_mhs');

Route::post('/prosesEdit', [App\Http\Controllers\MahasiswaController::class,'prosesEdit'])->name('edit');

Route::get('/prosesHapus{id}', [App\Http\Controllers\MahasiswaController::class,'prosesHapus'])->name('hapus');

