@extends('layouts.main')

@section('nav_right')
    <li><a href="{{ route('tampil_mhs') }}">Data Mahasiswa</a></li>
    <li class="active">Edit mahasiswa</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Edit</strong> Data Mahasiswa
          </div>
          <div class="card-body card-block">
            @foreach ($data_mhs as $data)
                <form action="{{ route('edit') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }} 
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIM</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nim_mhs" placeholder="Masukan NIM anda..." class="form-control" value="{{ $data->nim_mahasiswa }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Lengkap</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama_mhs" placeholder="Masukan nama lengkap anda..." class="form-control" value="{{ $data->nama_mahasiswa }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kelas</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="kelas_mhs" placeholder="Masukan kelas anda..." class="form-control" value="{{ $data->kelas_mahasiswa }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Program Studi</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="prodi_mhs" placeholder="Masukan program studi anda..." class="form-control"  value="{{ $data->prodi_mahasiswa }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="jurusan_mhs" placeholder="Masukan jurusan anda..." class="form-control" value="{{ $data->jurusan_mahasiswa }}"></div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm">Batal</button>
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endsection
