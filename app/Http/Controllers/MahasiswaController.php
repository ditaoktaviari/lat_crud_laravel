<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class MahasiswaController extends Controller
{
    public function tampilMahasiswa(){
        $mhs = DB::table('mahasiswa')->get();
        return view('mahasiswa',['data_mhs'=>$mhs]);
    }

    public function tambahMahasiswa(){
        return view('tambah_mhs');
    }

    public function prosesTambah(Request $request){
        DB::table('mahasiswa')->insert([
            'nim_mahasiswa'=>$request->nim_mhs,
            'nama_mahasiswa'=>$request->nama_mhs,
            'kelas_mahasiswa'=>$request->kelas_mhs,
            'prodi_mahasiswa'=>$request->prodi_mhs,
            'jurusan_mahasiswa'=>$request->jurusan_mhs
        ]);

        return redirect('/');
    }

    public function getId ($id){
        $mhs = DB::table('mahasiswa')->where('id',$id)->get();
        return view('edit_mhs',['data_mhs'=>$mhs]);
    }

    public function prosesEdit(Request $request){
        DB::table('mahasiswa')->where('id',$request->id)->update([
            'nim_mahasiswa'=>$request->nim_mhs,
            'nama_mahasiswa'=>$request->nama_mhs,
            'kelas_mahasiswa'=>$request->kelas_mhs,
            'prodi_mahasiswa'=>$request->prodi_mhs,
            'jurusan_mahasiswa'=>$request->jurusan_mhs
        ]);

        return redirect('/');
    }

    public function prosesHapus($id){
        DB::table('mahasiswa')->where('id',$id)->delete();
        return redirect('/');
    }
}
